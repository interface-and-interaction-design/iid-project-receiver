# Interface and interaction design Project Design A

## Project setup
```
node index.js
```

### Dockerize
```
docker build -t iid-receiver .
docker run -it -p 8081:8081 --rm --name iid-receiver-1 iid-receiver
```

### Podman
```
podman build -t iid-receiver .
podman run -it -p 8081:8081 --rm --name iid-receiver-1 iid-receiver
```