const express = require('express');
const app = express();
const port = 8081;
const cors = require('cors')
const bodyParser = require('body-parser');
app.use(bodyParser.json());

const environment = process.env.NODE_ENV
const isDevelopment = environment === 'dev'

if (isDevelopment) {
    console.log('Starting in development mode.')
    app.use(cors())
}
let items = [];

app.get('/', (req, res) => {
    if (items.length > 0) {
        res.send(items.pop());
    } else {
        res.send('{}');
    }
})

app.post('/', (req, res) => {
    console.log('Received: ');
    console.log(req.body)
    items.push(req.body);
    res.send('Request received!');
})


app.listen(port, () => {
    console.log(`Receiver app listening at http://localhost:${port}`);
})